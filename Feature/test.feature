Feature: Launch a Browser

Scenario: launch browser
When I launch a browser
Then title should be displayed
And Enter Address "Thane, Maharashtra, India" and company name as "ABC"
And Enter Fleet Mode as "Multi-Vechicle" with "3" vechiles and capacity as "10" per vechicle

Scenario Outline: User Enter Data for Order
When User enter username as "<Name>" , address as "<Address>" and phone number as "<Number>"
Then It Should be added in order
Examples: 
      | Name          | Address | Number |
      | Rahul |  Ambernath East, Ambarnath, Maharashtra, India | 8888884444 |
      | Sachin | Kalyan, Maharashtra, India | 8888889999 |
      | Vivek  | Ulhasnagar, Maharashtra, India | 8888889998 |

Scenario:  Tour Details Page Verification
Given We are on tour details Page
Then Verify order number displayed
And Verify Address Displayed      

Scenario:  Add CSV file with 3 orders
Given We are on Add Order Page with no order added
And Read CSV File to get an actual data
When Import and Upload CSV file with Order   
Then Order should be added and plan the tour
And Verify Address Displayed on tour page  