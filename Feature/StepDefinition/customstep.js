const {When, Then, Given} = require('@cucumber/cucumber')
const assert = require("assert")
const homepage = require ('../../Library/Base/HomePage.js');
const orderpage = require ('../../Library/Base/OrderPage.js');
const tourpage = require ('../../Library/Base/TourPage.js');
const readCSV = require ('../../Library/Base/ReadCsvFile.js');
let array;
let arrayCsvValue=[];

// When('i launch a browser', function () {
//    homepage.go_to_url("https://wegodeliver.here.com/");
// });

// Then('title should be displayed', function () {

// });  

When('I launch a browser', async () => {
    var url = "https://wegodeliver.here.com"
    await homepage.go_to_url("https://wegodeliver.here.com");
    
});

Then('title should be displayed', {timeout: 60 *10000} ,async () => {
    await homepage.tutorialskipandcookies();
});


Then('Enter Address {string} and company name as {string}',{timeout: 70 *10000}, async (string, string2) => {
    await homepage.setNameAndAddress(string, string2);
});

Then('Enter Fleet Mode as {string} with {string} vechiles and capacity as {string} per vechicle', {timeout: 70 *10000}, async (string, string2, string3) => {
    await homepage.setModeOfFleet(string, string2, string3);
});

When('User enter username as {string} , address as {string} and phone number as {string}', {timeout: 70 *10000}, async (string, string2, string3) => {
    arrayValue = await orderpage.add_manual_order(string, string2, string3);
    console.log(arrayValue);

    // await orderpage.addOrderThroughImport("/Users/rahulmac/Downloads/RahulSardar/HereAssignmentJavaScript/Data/Final_Test_Here.csv");
    // arrayCsvValue=   await readCSV.readFile("/Users/rahulmac/Downloads/RahulSardar/HereAssignmentJavaScript/Data/Final_Test_Here.csv");
    // console.log(arrayCsvValue);

});

Then('It Should be added in order',  {timeout: 70 *10000}, async () => {
    let returnValue;
    returnValue= await orderpage.add_to_order();
    console.log(returnValue);
    assert.deepEqual(returnValue,arrayValue);
    
});



Given('We are on tour details Page', {timeout: 70 *10000}, async () => {
    var header = await tourpage.planTour();
    assert.equal(header,"Tour details");
   
});


Then('Verify order number displayed', {timeout: 70 *10000}, async () => {
    var actualorders = await tourpage.ordersontourpage();
    var expectedOrders = await orderpage.ordercount();
    assert.equal(actualorders,expectedOrders);
    // await orderpage.addOrderThroughImport("/Users/rahulmac/Downloads/RahulSardar/HereAssignmentJavaScript/Data/Final_Test_Here.csv");
    // await readCSV.readFilefromcsv();

});  

Then('Verify Address Displayed', {timeout: 80 *10000}, async () => {
    await tourpage.tourDetails(1);
    await tourpage.tourDetails(2);
    await tourpage.tourDetails(3);
    var expectedValueDisplayed = await orderpage.addressNameExpectedtobeDisplayedonTourPage();
    console.log(expectedValueDisplayed);
    var actualValueDisplayed = await tourpage.addressDisplayedonTourPage();
    console.log(actualValueDisplayed);


    // sort by key
    var mapSort3 =  new Map([...expectedValueDisplayed.entries()].sort());
   
    var mapSort4 = new Map([...actualValueDisplayed.entries()].sort());
    assert.deepEqual(mapSort3,mapSort4);
    
   
});



Given('We are on Add Order Page with no order added', {timeout: 80 *10000}, async () => {
    await tourpage.gotoAddOrder();
    await orderpage.removeAllOrder();
    
});

Given('Read CSV File to get an actual data', {timeout: 80 *10000}, async () => {
    await readCSV.readFilefromcsv();
});

When('Import and Upload CSV file with Order', {timeout: 80 *10000}, async () => {
    await orderpage.addOrderThroughImport("/Users/rahulmac/Downloads/RahulSardar/HereAssignmentJavaScript/Data/Final_Test_Here.csv");
});

Then('Order should be added and plan the tour', {timeout: 80 *10000}, async () => {
    var expectedUploadedValues = await readCSV.readNameAndAddress();
    var mapExpected =  new Map([...expectedUploadedValues.entries()].sort());
    
    var mapDisplayed = await orderpage.readOrderAddedFromImport(mapExpected.size+"");
    var mapActual =  new Map([...mapDisplayed.entries()].sort());
    assert.deepEqual(mapExpected,mapActual);
   
});


Then('Verify Address Displayed on tour page', {timeout: 80 *10000}, async () => {
    var header = await tourpage.planTour();
    assert.equal(header,"Tour details");

    await tourpage.tourDetails(1);
    await tourpage.tourDetails(2);
    await tourpage.tourDetails(3);
    var expectedUploadedValues = await readCSV.readNameAndAddress();
    var mapExpected =  new Map([...expectedUploadedValues.entries()].sort());
    console.log(mapExpected);
    // for(let i=1; i<=mapExpected.size; i++){
    //     await tourpage.tourDetails(i);
    // }

    var actualValueDisplayed = await tourpage.addressDisplayedonTourPage();
    var mapActual = new Map([...actualValueDisplayed.entries()].sort());
    console.log(mapActual);
    assert.deepEqual(mapExpected,mapActual);
});


