const {By,Key,Builder} = require("selenium-webdriver");
require("chromedriver");
var basePage = require ('./BasePage.js');
var webdriver = require("selenium-webdriver");
const csv = require('csv-parser');
const fs = require('fs');
let array =[];
var id =0;
var map1 = new Map();
class OrderPage extends basePage{

    async add_manual_order(name, address , phone){
        //const arrName = new Array(name,address);
        await driver.findElement(By.id("button-orders-mode-Manual")).click();
        await driver.findElement(By.id("input-edit-order-demand")).sendKeys(Key.BACK_SPACE);
        await driver.findElement(By.id("input-edit-order-demand")).sendKeys("1");
        await driver.findElement(By.id("input-edit-order-name")).sendKeys(name);
        // await driver.findElement(By.id("input-edit-order-add")).sendKeys(address);
        await driver.findElement(By.id("input-edit-order-phone")).sendKeys(phone);
        await driver.findElement(By.id("input-edit-order-add")).sendKeys(address);
        //await driver.findElement(By.id("button-add-order-manual")).click();
        // var map = webdriver.promise.map;

        // var elems = driver.findElements(By.xpath("//div[@id='input-edit-order-add-options']/div"));
        // map(elems, e => e.getText())
        // .then(function(values) {
        //     console.log(values);
        // });

        // driver.findElements(webdriver.By.xpath(my_xpath)).then(function(elements) {
        //     for (var i = 0; i < elements.length; i++) {
        //         elements[i].getText().then(function(text) {
        //             console.log(text);
        //         })
        //     };
        // });
        await driver.sleep(5000);
        await driver.findElements(webdriver.By.xpath("//div[@id='input-edit-order-add-options']/div")).then(function(elements) {
            elements[0].click();
        });
       
        //await driver.findElement(By.id("button-add-order-manual")).click();
        map1.set(name,address);
        return [name, address]        
    }
    async add_to_order(){
       
        await driver.findElement(By.id("button-add-order-manual")).click();
        await driver.executeScript("arguments[0].scrollIntoView(true);",driver.findElement(By.xpath("//div[@class='css-1be61dj']")));
        var nameXpathPart = "(//div[@id='label-order-name-" + id.toString() + "']/div/div[@class='css-mudjgk'])[1]" ;
        var addressXpathPart = "(//div[@id='label-order-address-" + id.toString() + "']/div[@class='css-1g94hb7'])[1]" ;
        var name = await driver.findElement(By.xpath(nameXpathPart)).getText();
        var address = await driver.findElement(By.xpath(addressXpathPart)).getText();
        id++;
        await driver.executeScript("arguments[0].scrollIntoView(true);",driver.findElement(By.id("input-edit-order-name")));
        return [name, address]
    }

    async ordercount(){
        return id
    }

    async addressNameExpectedtobeDisplayedonTourPage() {
        return map1
    }

    async addOrderThroughImport(path){
        await driver.findElement(By.id("button-orders-mode-Import")).click();
        await driver.findElement(By.id("input-file-upload")).sendKeys("/Users/rahulmac/Downloads/RahulSardar/HereAssignmentJavaScript/Data/Final_Test_Here.csv");
        await driver.findElement(By.xpath("//strong[text()='Save']")).click();
        await driver.findElement(By.xpath("//strong[text()='Confirm']")).click();
        await driver.sleep(5000);
    }

    async removeAllOrder(){
        await driver.sleep(5000);
        await driver.executeScript("arguments[0].scrollIntoView(true);",driver.findElement(By.id("link-remove-orders")));
        await driver.findElement(By.id("link-remove-orders")).click();
        await driver.executeScript("arguments[0].scrollIntoView(true);",driver.findElement(By.id("button-orders-mode-Import")));

    }

    async readOrderAddedFromImport(count){
        var map1 = new Map();
        for(let i=0; i<parseInt(count); i++){
            var nameXpathPart = "(//div[@id='label-order-name-" + i.toString() + "']/div/div[@class='css-mudjgk'])[1]" ;
            var addressXpathPart = "(//div[@id='label-order-address-" + i.toString() + "']/div[@class='css-1g94hb7'])[1]" ;
            await driver.executeScript("arguments[0].scrollIntoView(true);",driver.findElement(By.xpath(nameXpathPart)));
            await driver.sleep(3000);
            var name = await driver.findElement(By.xpath(nameXpathPart)).getText();
            var address = await driver.findElement(By.xpath(addressXpathPart)).getText();
            map1.set(name,address);
        }
        return map1;
    }

}   

module.exports = new OrderPage();