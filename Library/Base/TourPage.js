const {By,Key,Builder} = require("selenium-webdriver");
require("chromedriver");
var basePage = require ('./BasePage.js');
var webdriver = require("selenium-webdriver");
var map2 = new Map();

class TourPage extends basePage{
    async planTour(){
       
        await driver.findElement(By.id("button-orders-next")).click();
        await driver.sleep(10000);
        let textHeader = await driver.findElement(By.xpath("//h1[@class='css-17m1rwz']")).getText();
        return textHeader
    }  

    async ordersontourpage(){
        let orders = await driver.findElement(By.xpath("(//strong[@class='css-ta9a01'])[2]")).getText();
        return orders
    }  

    async tourDetails(number){
        var orderXpath = "(//div[@class]//div[text()='" + number.toString() + "'])[1]" ;
        await driver.executeScript("arguments[0].scrollIntoView(true);",driver.findElement(By.xpath(orderXpath)));
        await driver.findElement(By.xpath(orderXpath)).click();
        var address = await driver.findElement(By.xpath("//div[@class='css-wuv9sq']/div[@class='css-1envc9j']")).getText();
        let links = await driver.findElements(By.xpath("//div[@class='css-1mjfjpg']/div[@class='css-7voh9a']"));
        for(let link of links) {
            await driver.sleep(5000);
            await driver.executeScript("arguments[0].scrollIntoView(true);",driver.findElement(By.xpath("//div[@class='css-1mjfjpg']/div[@class='css-7voh9a']")));
            var text = await link.getText();
            await map2.set(text,address);
            //console.log('Address and name displayed for order number %i on tour details page is ' ,number,address,text);
        } 
    } 

    async addressDisplayedonTourPage(){
         return map2
    }  
       
    async gotoAddOrder(){
        await driver.findElement(By.id("button-tour-details-back")).click();    
    }   
       
   
}
module.exports = new TourPage();