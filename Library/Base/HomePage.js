const {By,Key,Builder} = require("selenium-webdriver");
require("chromedriver");
var basePage = require ('./BasePage.js');
var webdriver = require("selenium-webdriver");

class Homepage extends basePage{
     async tutorialskipandcookies(){
        
         await driver.findElement(By.xpath("//*[text()='Skip']")).click();
        
         await driver.findElement(By.xpath("//*[text()='Accept cookies']")).click();
       }  

       async setNameAndAddress(address,name){  
        await driver.findElement(By.xpath("//button[@class='css-vj0a1q']")).click(); 
        driver.sleep(10000);
        await driver.findElement(By.xpath("//div[@class='css-1gcz17a']/input")).click(); 
        await driver.findElement(By.xpath("//div[@class='css-1gcz17a']/input")).sendKeys(name); 
        await driver.findElement(By.xpath("//input[@id='input-depot-loca']")).sendKeys(address); 
        await driver.sleep(5000);
        await driver.findElements(webdriver.By.xpath("//div[@id='input-depot-loca-options']/div")).then(function(elements) {
          elements[0].click();
        });

        //await driver.sleep(5000);
        //await driver.executeScript("arguments[0].scrollIntoView(true);",driver.findElement(By.xpath("//strong[text()='Capacity per vehicle']")));
        // await driver.findElement(By.xpath("//select[@class='css-1ygw2g1']")).click();

        // if(mode.toLowerCase()== "solo") {
        //   await driver.findElement(By.xpath("//option[@value='solo']")).click();

        // }else{
        //   await driver.findElement(By.xpath("//option[@value='Multi-Vehicle']")).click();
        // }  
      }     
      
      async setModeOfFleet(mode,vechiles,capacity){
        var numofvechicle = vechiles || "2";
        var percapacity = capacity || "10";
        await driver.executeScript("arguments[0].scrollIntoView(true);",driver.findElement(By.xpath("//strong[text()='Capacity per vehicle']")));
        if(mode.toLowerCase()== "solo") {
          await driver.findElement(By.xpath("//select[@class='css-1ygw2g1']")).click();
          await driver.findElement(By.xpath("//option[@value='solo']")).click();

        }else{
          await driver.findElement(By.xpath("//select[@class='css-1ygw2g1']")).click();
          await driver.findElement(By.xpath("//option[text()='Multi-Vehicle']")).click();
         
          var fieldLengthCapacity =3;
          while (fieldLengthCapacity!=0) {
            await driver.findElement(By.xpath("//input[@id='input-fleet-capacity']")).sendKeys(Key.BACK_SPACE); 
            fieldLengthCapacity--;
          }
          await driver.findElement(By.xpath("//input[@id='input-fleet-capacity']")).sendKeys(capacity); 

          var fieldLengthAmount =3;
          while (fieldLengthAmount!=0) {
            await driver.findElement(By.xpath("//input[@id='input-fleet-amount']")).sendKeys(Key.BACK_SPACE); 
            fieldLengthAmount--;
          }
          await driver.findElement(By.xpath("//input[@id='input-fleet-amount']")).sendKeys(numofvechicle); 
          await driver.findElement(By.id("button-fleet-next")).click();

        }  
      }  
        
    
}
module.exports = new Homepage();